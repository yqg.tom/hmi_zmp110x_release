// https://github.com/itas109/CSerialPort/blob/master/src/SerialPortInfoWinBase.cpp

#include <iostream>

#include <string>
#include <vector>

#include "windows.h" // CreateFile GetTickCount64
#include "tchar.h" // _sntprintf _T 

#include <Setupapi.h> //SetupDiGetClassDevs Setup*
#include <initguid.h> //GUID

#include "pc_uart_debug.h"

#pragma comment (lib, "setupapi.lib")

using namespace std;

#ifndef GUID_DEVINTERFACE_COMPORT
DEFINE_GUID(GUID_DEVINTERFACE_COMPORT, 0x86E0D1E0L, 0x8089, 0x11D0, 0x9C, 0xE4, 0x08, 0x00, 0x3E, 0x30, 0x1F, 0x73);
#endif

struct SerialPortInfo
{
    std::string portName;
    std::string description;
};

std::string wstringToString(const std::wstring& wstr)
{
    // https://stackoverflow.com/questions/4804298/how-to-convert-wstring-into-string
    if (wstr.empty())
    {
        return std::string();
    }

    int size = WideCharToMultiByte(CP_ACP, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string ret = std::string(size, 0);
    WideCharToMultiByte(CP_ACP, 0, &wstr[0], (int)wstr.size(), &ret[0], size, NULL, NULL); // CP_UTF8

    return ret;
}

bool enumDetailsSerialPorts(vector<SerialPortInfo>& portInfoList)
{
    // https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdienumdeviceinfo

    bool bRet = false;
    SerialPortInfo m_serialPortInfo;

    std::string strFriendlyName;
    std::string strPortName;

    HDEVINFO hDevInfo = INVALID_HANDLE_VALUE;

    // Return only devices that are currently present in a system
    // The GUID_DEVINTERFACE_COMPORT device interface class is defined for COM ports. GUID
    // {86E0D1E0-8089-11D0-9CE4-08003E301F73}
    hDevInfo = SetupDiGetClassDevs(&GUID_DEVINTERFACE_COMPORT, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

    if (INVALID_HANDLE_VALUE != hDevInfo)
    {
        SP_DEVINFO_DATA devInfoData;
        // The caller must set DeviceInfoData.cbSize to sizeof(SP_DEVINFO_DATA)
        devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

        for (DWORD i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, &devInfoData); i++)
        {
            // get port name
            TCHAR portName[256];
            HKEY hDevKey = SetupDiOpenDevRegKey(hDevInfo, &devInfoData, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_READ);
            if (INVALID_HANDLE_VALUE != hDevKey)
            {
                DWORD dwCount = 255; // DEV_NAME_MAX_LEN
                RegQueryValueEx(hDevKey, _T("PortName"), NULL, NULL, (BYTE*)portName, &dwCount);
                RegCloseKey(hDevKey);
            }

            // get friendly name
            TCHAR fname[256];
            SetupDiGetDeviceRegistryProperty(hDevInfo, &devInfoData, SPDRP_FRIENDLYNAME, NULL, (PBYTE)fname,
                sizeof(fname), NULL);

#ifdef UNICODE
            strPortName = wstringToString(portName);
            strFriendlyName = wstringToString(fname);
#else
            strPortName = std::string(portName);
            strFriendlyName = std::string(fname);
#endif
            // remove (COMxx)
            strFriendlyName = strFriendlyName.substr(0, strFriendlyName.find(("(COM")));

            m_serialPortInfo.portName = strPortName;
            m_serialPortInfo.description = strFriendlyName;
            portInfoList.push_back(m_serialPortInfo);
        }

        if (ERROR_NO_MORE_ITEMS == GetLastError())
        {
            bRet = true; // no more item
        }
    }

    SetupDiDestroyDeviceInfoList(hDevInfo);

    return bRet;
}
extern "C" void get_uart_info(com_info_t*);

void get_uart_info(com_info_t* com_info)
{
    vector<SerialPortInfo> spInfo;
    enumDetailsSerialPorts(spInfo);

    int *com_num = (int *)malloc(sizeof(int) * spInfo.size());
    com_name_t *get_uart_com = (com_name_t *)malloc(sizeof(com_name_t) * spInfo.size());

    for (int i = 0; i < spInfo.size(); i++) {
        com_num[i] = atoi(spInfo[i].portName.substr(3).c_str());
    }

    for (int i = 0, j = 0; i < spInfo.size(); i++) {

        int min = 0xFF;
        int j_min = 0;
        for (j = 0; j < spInfo.size(); j++) {
            if (min > com_num[j]) {
                min = com_num[j];
                j_min = j;
            }
        }
        com_num[j_min] = 0xFF;

        strcpy(get_uart_com[i].com, spInfo[j_min].portName.c_str());
        strcpy(get_uart_com[i].com_name, spInfo[j_min].description.c_str());
    }
    free(com_num);
    com_info->com_count = spInfo.size();
    com_info->com_info = get_uart_com;
}
