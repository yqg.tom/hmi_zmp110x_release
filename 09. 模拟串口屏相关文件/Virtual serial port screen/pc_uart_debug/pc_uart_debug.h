#ifndef PC_URAT_DEBUG_H
#define PC_URAT_DEBUG_H

#include <stdio.h>

#define CHECK_INTERVAL 10       //定时查询串口是否有数据，单位：ms

#define PC_UART_BAUDRATE            115200
#define PC_UART_BYTESIZE            8
#define PC_UART_PARITY              1
#define PC_UART_STOPBITS            0
#define PC_UART_FLOWCONTROL         0


typedef struct  _com_name_t
{
    char com[10];
    char com_name[50];
}com_name_t;

typedef struct  _com_info_t
{
    int com_count;
    com_name_t* com_info;
}com_info_t;



#endif PC_URAT_DEBUG_H
